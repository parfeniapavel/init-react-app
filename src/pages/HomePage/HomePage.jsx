import React, { Fragment } from 'react'

export default ({ user }) => {
  return (
    <Fragment>
      Hello, {user.userData}
    </Fragment>
  )
}