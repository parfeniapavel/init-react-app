
/*                    USER-ACTIONS                                               */
export const ACTION_USER_LOGIN = 'ACTION_USER_LOGIN'
export const ACTION_USER_LOGIN_SUCCESS = 'ACTION_USER_LOGIN_SUCCESS'
export const ACTION_USER_LOGIN_ERROR = 'ACTION_USER_LOGIN_ERROR'

export const ACTION_USER_LOGOUT_SUCCESS = 'ACTION_USER_LOGOUT_SUCCESS'

export const ACTION_USER_SIGN_IN_SUCCESS = 'ACTION_USER_SIGN_IN_SUCCESS'
export const ACTION_USER_SIGN_IN_ERROR = 'ACTION_USER_SIGN_IN_ERROR'
/*                    /USER-ACTIONS                                               */