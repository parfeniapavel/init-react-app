import React, { Fragment } from 'react'
import { Switch } from 'react-router-dom'
import { PrivateRoute, PublicRoute } from './components'

import { HomePage } from './pages'

export default () => {
  return (
    <Fragment>
      <Switch>
        <PublicRoute restricted={true} path='/' component={ HomePage } />
      </Switch>
    </Fragment>
  )
}